#include <ctime>
#include <cstdio>
#include <exception>
#include <list>
#include <cpp4scripts/c4slib.hpp>

#define MENACON_MICROLOG_BUILD
#include "micrologger.h"

using namespace c4s;

program_arguments args;

class LogFile
{
public:
    LogFile() {
        fp = fopen("microlog.bin", "rb");
        if (!fp) {
            throw rterror("Unable to open 'microlog.bin'.");
        }
        fread(&start_time, sizeof(time_t), 1, fp);
        fread(&idval, sizeof(int), 1, fp);

        printf("microlog service started: %s", ctime(&start_time));
        if (idval != MICROLOG_SERVER_ID) {
            fclose(fp);
            fp = 0;
            throw rterror("Error: server signature not found from log.");
        }
    }
    ~LogFile() {
        if (fp)
          fclose(fp);
    }
    bool getItem() {
        return fread(&item, sizeof(MicroLogger::LogItem), 1, fp) == 0 ? false : true;
    }
    bool tail(int count) {
        return fseek(fp, count * sizeof(MicroLogger::LogItem), SEEK_END ) ? false : true;
    }

    time_t start_time;
    MicroLogger::LogItem item;

protected:
    FILE* fp;
    int idval;
};

struct IndexItem
{
    int ndx;
    ntbs_md file;
    ntbs_md function;
};

int print_min()
{
    char ts_str[32];
    LogFile log;

    printf("Printing first and last entries from the binary log:\n");
    for (int ndx = 0; ndx < 20; ndx++) {
        log.getItem();
        strftime(ts_str, sizeof(ts_str), "%X", localtime(&log.item.ts.tv_sec));
        printf("%s.%09ld | %d\n", 
               ts_str,
               log.item.ts.tv_nsec, 
               log.item.value);
    }
    printf("...\n");
    if (log.tail(20))
        return 1;
    for (int ndx = 0; ndx < 20; ndx++) {
        log.getItem();
        strftime(ts_str, sizeof(ts_str), "%X", localtime(&log.item.ts.tv_sec));
        printf("%s.%09ld | %d\n", 
               ts_str,
               log.item.ts.tv_nsec, 
               log.item.value);
    }    
    return 0;
}

int print_full()
{
    char line[2048];
    char ts_str[32];
    std::list<IndexItem> index;
    IndexItem item;
    time_t caltime;
    time_t seconds;
    time_t first_sec = 0;
    const char* index_name = 0;

    if (args.is_set("-i"))
        index_name = args.get_value("-i").get();
    else
        index_name = "microlog.index";

    // Read the instrumented location index into memory
    FILE* f_ndx = fopen(index_name, "r");
    if (!f_ndx) {
        throw rterror("Unable to open 'microlog.index'.");
    }

    //: 4; src/driver/mgp/duplicatedop.cpp:48; void DuplicateDop::init()
    while(fgets(line, sizeof(line), f_ndx)) {
        item.ndx = strtol(strtok(line, ";"), 0, 10);
        item.file = strtok(0, ";");
        item.function = strtok(0, "\n");
        index.push_back(item);
    }
    if (ferror(f_ndx)) {
        throw rterror("Reading location index failed: %s\n", strerror(errno));
    }
    fclose(f_ndx);

    LogFile log;
    FILE* out = fopen("microlog.log", "w");
    if (!out)
        throw rterror("Unable to open export log 'microlog.log'");

    printf("Creating export log: microlog.log\n");;
    fprintf(out, "Timestamp | seconds | location | file:line | function\n"
                 "----------------------------------------------------\n");
    while (log.getItem()) {
        seconds = log.item.ts.tv_sec - log.start_time;
        if (!first_sec)
            first_sec = seconds;
        caltime = log.start_time + seconds;
        strftime(ts_str, sizeof(ts_str), "%X", localtime(&caltime));
        for (auto ii : index) {
            if (ii.ndx == log.item.value) {
                fprintf(out, "%s.%09ld | % 4ld | % 4d | %s | %s\n", 
                    ts_str,
                    log.item.ts.tv_nsec,
                    seconds - first_sec,
                    log.item.value,
                    ii.file.get(), 
                    ii.function.get());
                break;
            }
        }
    }    
    fclose(out);
    printf("\nDone\n");
    return 0;
}

int instrument_file(const path& sp, FILE* index, int& counter)
{
    printf("Instrumenting %s\n", sp.get_ccbase());

    path tn(sp);
    tn.set_base("microlog_tmp.cxx");
    FILE* source = fopen(sp.get_ccpath(), "r");
    FILE* tmpf = fopen(tn.get_ccpath(), "w");

    enum { BEGIN, INCLUDE, BETWEEN, SKIP, FUNCTION } state = BEGIN;
    ntbs_xl line;
    ntbs_md function;
    bool ignore_namespace=false;
    int ch;
    int lc = 1;
    while((ch = fgetc(source)) != EOF) {
        line += ch;
        if (state == SKIP && ch == '}') {
            state = BETWEEN;
        }
        if (ch == '\n') {
            switch(state) {
            case BEGIN:
                // If first include found:
                if (!strncmp("#include", line.get(), 8)) {
                    state = INCLUDE;
                    break;
                }
                if (lc > 50) {
                    printf("Warning: insturment_file - unable to find includes in %s", sp.get_ccpath());
                    fclose(source);
                    fclose(tmpf);
                    tn.rm();
                    return 0;
                }
                break;
            case INCLUDE:
                // First break in includes
                if (strncmp("#include", line.get(), 8)) {
                    if (!strncmp(line.get(), "namespace", 9))
                        ignore_namespace = true;   
                    line.prepend("\n#include <micrologger.h>\n"
                                 "extern MicroLogger mlog;\n\n", 52);
                    lc += 4;
                    state = BETWEEN;
                }
                break;
            case BETWEEN:
                if (!strncmp(line.get(), "class", 5) ||
                    !strncmp(line.get(), "enum", 4) ||
                    !strncmp(line.get(), "struct", 6))
                {
                    state = SKIP;
                }
                else if (!strncmp(line.get(), "namespace", 9)) {
                    if (line.find(';') == SIZE_MAX)
                        ignore_namespace = true;
                }
                else if (line.get(0) == '{') {
                    if (ignore_namespace) {
                        ignore_namespace = false;
                        break;
                    }
                    if (line.find('}') != SIZE_MAX) {
                        ntbs_sm al;
                        al.sprint("mlog.addLocation(%d);", counter);
                        line.insert(1, al);
                        // Since this is one-line-function we do not change state.
                    } else {
                        line.addprint("    mlog.addLocation(%d);\n", counter);
                        lc++;
                        state = FUNCTION;
                    }
                    fprintf(index, "%d; %s:%d; %s\n", 
                        counter, sp.get_ccpath(), lc, function.get());
                    counter++;
                } else if (line.get(0) != '\n' && line.get(0) != ' ') {
                    // parse potential function name
                    size_t pos = line.find('(');
                    if (pos == SIZE_MAX)
                        break;
                    function = line.substr(0, pos);
                    pos = function.rfind(' ', 1);
                    if (pos != SIZE_MAX)
                        function.erase(0, pos+1);
                }
                break;
            case FUNCTION:
                if (line.get(0) == '}')
                    state = BETWEEN;
                break; 
            }
            fputs(line.get(), tmpf);
            line.clear();
            lc++;
        }
    }

    fputs(line.get(), tmpf);
    fclose(source);
    fclose(tmpf);

    if (args.is_set("-bo")) {
        path copy(sp);
        copy.set_base(sp.get_base_plain() + ntbs("~ml") + sp.get_ext());
        sp.cp(copy, PCF_MOVE|PCF_FORCE);
        tn.rename(sp.get_base());
    } else {
        tn.cp(sp, PCF_MOVE|PCF_FORCE);
    }
    return 0;
}

int instrument_directory()
{
    int rv = 0;
    int counter = 1;
    path target;
    char tail[256];

    FILE *index = fopen("microlog.index", "a+");
    if (!index)
        throw rterror("Unable to create / open 'microlog.index' file.");
    fseek(index, -(sizeof(tail)-1), SEEK_END);
    long fpos = ftell(index);
    if (fpos > 0) {
        fread(tail, sizeof(tail)-1, 1, index);
        tail[sizeof(tail)-2] = 0;
        char* lf = strrchr(tail, '\n');
        counter = strtol(lf+1, 0, 10);
        counter++;
        printf("Found existing index. Starting from %d.\n", counter);
    } else
        printf("Starting new 'microlog.index' file.\n");

    if (args.is_set("-s")) {
        target = args.get_value("--instrument");
        instrument_file(target, index, counter);
        printf("File instrumented. Counter at %d\n", counter);
    } else {
        ntbs regex = args.is_set("-regex") ? args.get_value("-regex") : ntbs("\\.cpp$");
        target.set_dir(args.get_value("--instrument"));
        path_list cppfiles(target, regex, args.get_value("-exclude"), PLF_SYML);
        for (auto cpp : cppfiles) {
            rv = instrument_file(cpp, index, counter);
            if (rv)
                break;
        }

        printf("Directory instrumented.\nCounter at: %d\n", counter-1);
    }

    fclose(index);
    return rv;
}

int main(int argc, char const *argv[])
{
    int rv = 0;
    args += argument("--min",    false, "Makes minimal text representation of the given log file");
    args += argument("--full",   false, "Combines binary location log with index to full log output");
    args += argument("--instrument", true, "Directory name to instrument (Use -s if this is single file)");
    args += argument("-i",       true,  "Use named index file instead of microlog.index in current directory");
    args += argument("-s",       false, "Instrument single file instead of directory. (Regex and exclude are ignored.)");
    args += argument("-regex",   true, "Instruments only files that match the given regular expression");
    args += argument("-exclude", true, "Regex for files to exclude");
    args += argument("-bo",      false, "Backup original. Original file will have '~ml' appended to the file name.");

    try {
        printf("Menacon Microlog utility %s\n", VERSION);
        args.initialize(argc, argv);
    } catch(const c4s_exception &ce){
        printf("Argument initialization failure: %s\n",ce.what());
        return 1;
    }

    try {
        if (args.is_set("--min")) {
            rv = print_min();
        } else if (args.is_set("--full")) {
            print_full();
        } else if (args.is_set("--instrument")) {
            instrument_directory();
        } else {
            fprintf(stderr,"Nothing to do\n");
            args.usage();
        }
    } catch(const c4s_exception& ce) {
        fprintf(stderr, "Instrumenting failed: %s\n", ce.what());
        rv = 99;
    }
    return rv;
}

#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>

#include <stdio.h>
#include <string.h>

#define MENACON_MICROLOG_BUILD
#include "micrologger.h"

volatile sig_atomic_t quit_signal = 0;
void signalHandler(int signum)
{
    if(signum == SIGUSR1 || signum == SIGHUP || signum == SIGTERM) {
        quit_signal = 1;
        printf("received quit signal\n");
    }
    else {
        signal(signum, SIG_DFL);
        raise(signum);
    }
}

MicroLogger::LogItem litems[8000];

const int MAX_PIPES = 5;

void closePipes(int *pipe_list, char const *argv[])
{
    for (int px=1; px <= MAX_PIPES && pipe_list[px-1]; px++) {
        close(pipe_list[px-1]);
        unlink(argv[px]);
    }
}

int main(int argc, char const *argv[])
{
    unsigned int value = 0;
    ssize_t br;
    size_t ndx = 0;
    size_t events = 0;
    int pipe_list[MAX_PIPES];
    int pipe_max = 0;
    int px;

    memset(litems, 0, sizeof(litems));
    memset(pipe_list, 0, sizeof(pipe_list));

    if (argc < 2) {
        printf("Usage: micrologsrv [pipe name]\n");
        printf("       1-5 space separated pipe names allowed.\n");
        return 1;
    }

    printf("Starting MicroLog Server %s\n", VERSION);
    printf("Send USR1 or TERM signal to quit.\n");
    printf("pid = %d\n", getpid());

    for (px = 1; px <= MAX_PIPES && px < argc; px++) {
        if (mkfifo(argv[px], 0666) == -1) {
            printf("Fifo creation failed for '%s': %s\n", argv[px], strerror(errno));
            closePipes(pipe_list, argv);
            return 2;
        }
        pipe_list[px-1] = open(argv[px], O_RDONLY|O_NONBLOCK, 0);
        if (pipe_list[px-1] == -1) {
            printf("Opening for '%s' failed: %s\n", argv[px], strerror(errno));
            pipe_list[px-1] = 0;
            closePipes(pipe_list, argv);
            return 3;
        }
    }
    pipe_max = px-1;

    time_t now;
    time(&now);
    timespec dummy;
    if(clock_gettime(CLOCK_MONOTONIC_COARSE, &dummy) == -1) {
        fprintf(stderr, "Unable to get time: %s\n", strerror(errno));
        return 4;
    }

    if (signal(SIGUSR1, signalHandler) == SIG_ERR ||
        signal(SIGTERM, signalHandler) == SIG_ERR ||
        signal(SIGHUP,  signalHandler) == SIG_ERR) {
        printf("Unable to set signal handlers\n");
        return 6;
    }

    FILE* log = fopen("microlog.bin", "w");
    if (!log) {
        printf("Unable to open log 'microlog.bin': %s\n", strerror(errno));
        return 5;
    }
    // Write time and id
    fwrite(&now, sizeof(time_t), 1, log);
    value = MICROLOG_SERVER_ID;
    fwrite(&value, sizeof(int), 1, log);

    while (!quit_signal) {
        for (px = 0; px < pipe_max; px++) {
            br = read(pipe_list[px], &value, sizeof(int));
            if (br == -1) { 
                if (errno == EAGAIN)
                    continue;
                printf("Pipe read failed: %s\n", strerror(errno));
                printf("Closing server...\n");
                quit_signal = 1;
                break;
            }
            if (br == sizeof(int)) {
                if (value == MLSRV_EVENTS_FLUSH) {
                    fwrite(litems, ndx * sizeof(MicroLogger::LogItem), 1, log);
                    fflush(log);
                    ndx = 0;
                    continue;
                }
                litems[ndx].value = value;
                clock_gettime(CLOCK_MONOTONIC_COARSE, &litems[ndx].ts);
                ndx++;
                events++;
                if(ndx >= 8000) {
                    fwrite(litems, ndx * sizeof(MicroLogger::LogItem), 1, log);
                    ndx = 0;
                }
            }
        }
    }
    if(ndx)
        fwrite(litems, ndx * sizeof(MicroLogger::LogItem), 1, log);
    fclose(log);

    closePipes(pipe_list, argv);
    printf("Server closed. %ld events recorded.\n", events);
    return 0;
}   
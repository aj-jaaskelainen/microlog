//micrologger.h
#ifndef MENACON_MICROLOGGER_H
#define MENACON_MICROLOGGER_H

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#ifdef MENACON_MICROLOG_BUILD
const char* VERSION = "v0.4";
#endif

const unsigned int MICROLOG_SERVER_ID = 0xAAAA5555;

const unsigned int MLSRV_EVENTS_BEGIN = 0xFFFFFF00;
const unsigned int MLSRV_EVENTS_FLUSH = 0xFFFFFF01;

class MicroLogger
{
public:
    MicroLogger(const char* pipename, bool enabled = true) {
        if (!enabled) {
            pipe_id = -1;
            return;
        }
        pipe_id = open(pipename, O_WRONLY|O_NONBLOCK, 0);
        if (pipe_id == -1) {
            printf("Unable to open log pipe: %s\n%s\n",
                pipename, strerror(errno));
            _exit(99);
        }
    }
    ~MicroLogger() {
        if (pipe_id > 0) {
            unsigned int event = MLSRV_EVENTS_FLUSH;
            write(pipe_id, &event, sizeof(int));
            close(pipe_id);
        }
    }
    void addLocation(unsigned int loc) {
        if (pipe_id > 0)
            write(pipe_id, &loc, sizeof(int));
    }

    struct LogItem 
    {
        LogItem() {
            value = 0;
            memset(&ts, 0, sizeof(ts));
        }
        int value;
        timespec ts;
    };

protected:
    int pipe_id;
};


#endif

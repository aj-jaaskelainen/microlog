#Makefile

# ARCH=-arch arm64
STDLIB=-stdlib=libc++
#STDLIB=-stdlib=libstdc++
CC=clang++
CFLAGS=-std=c++17 $(STDLIB) -g -Wall\
 -fcxx-exceptions -fno-rtti\
 -I/usr/local/include\
 -Wno-switch

cwd := $(dir $(abspath $(firstword $(MAKEFILE_LIST))))
C4SLIB=-L/usr/local/lib-d -lc4s

.PHONY: all

all: mlserver microlog

mlserver: mlserver.cpp
	$(CC) $(CFLAGS) -o $@ $<

microlog: microlog.cpp
	$(CC) $(CFLAGS) -o $@ $< $(C4SLIB)

install:
	ln -s $(cwd)microlog /usr/local/bin/microlog
	ln -s $(cwd)mlserver /usr/local/bin/mlserver
	ln -s $(cwd)micrologger.h /usr/local/include
